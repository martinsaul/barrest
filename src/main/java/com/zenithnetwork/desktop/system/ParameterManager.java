package com.zenithnetwork.desktop.system;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;

public final class ParameterManager {

	private static final Logger log = LoggerFactory.getLogger(ParameterManager.class.getName());

	private final HashMap<String, String> parameters;
	
	private static ParameterManager instance;
	
	public static ParameterManager getInstance(){
		if(instance == null)
			throw new RuntimeException("Parameters were not inicialized.");
		return instance;
	}
	
	public static ParameterManager initialize(HashMap<String, String> init){
		instance = new ParameterManager(init);
		return instance;
	}

	private ParameterManager(HashMap<String, String> init){
		parameters = init;
	}
	
	public boolean isDefined(String x){
		return parameters.containsKey(x);
	}
	
	public String getStringParameter(String x){
		return parameters.get(x);
	}
	
	public int getIntegerParameter(String x){
		try{
			return Integer.parseInt(parameters.get(x));
		} catch(NumberFormatException e){
			log.error("Parameter: \""+x+"\" could not be parsed to Integer. [Value: "+parameters.get(x)+"]");
			return 1;
		}
	}

	public int getIntegerParameter(String x, int i) {
		try{
			return Integer.parseInt(parameters.get(x));
		} catch(NumberFormatException e){
			log.debug("Parameter: \""+x+"\" was not passed or invalid. Defaulting to: "+i+".");
			return i;
		}
	}

	public long getLongParameter(String x){
		try{
			return Long.parseLong(parameters.get(x));
		} catch(NumberFormatException e){
			log.error("Parameter: \""+x+"\" could not be parsed to Long. [Value: "+parameters.get(x)+"]");
			return -1;
		}
	}

	public long getLongParameter(String x, long i) {
		try{
			return Long.parseLong(parameters.get(x));
		} catch(NumberFormatException e){
			log.debug("Parameter: \""+x+"\" was not passed or invalid. Defaulting to: "+i+".");
			return i;
		}
	}

	public double getDoubleParameter(String x){
		try{
		return Double.parseDouble(parameters.get(x));
		} catch(NumberFormatException e){
			log.error("Parameter: \""+x+"\" could not be parsed to Double. [Value: "+parameters.get(x)+"]");
			return 1;
		}

	}
	
	public boolean getBooleanParameter(String x){
		try{
		return Boolean.parseBoolean(parameters.get(x));
		} catch(Exception e){
			log.error("Parameter: \""+x+"\" could not be parsed to Boolean. [Value: "+parameters.get(x)+"]");
			return false;
		}

	}
}
