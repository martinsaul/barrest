package com.zenithnetwork.desktop.system;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class ProgramManager {

	private static final Logger log = LoggerFactory.getLogger(ProgramManager.class.getName());
	private static FileInputStream lockCheck;

	public static void forceCloseWithError(String description){
		log.error("Error: "+description.replaceAll("\n", " "));
		JOptionPane.showMessageDialog(null, description, "Error", JOptionPane.ERROR_MESSAGE);
		System.exit(-1);
	}

	public static void forceCloseWithError(String description, Exception exception) {
		log.error("Error: "+description.replaceAll("\n", " "),exception);
		JOptionPane.showMessageDialog(null, description, "Error", JOptionPane.ERROR_MESSAGE);
		System.exit(-1);
	}

	public static void forceClose() {
		log.warn("Force closing application.");
		System.exit(0);
	}
	
	public static void close() {
		System.exit(0);
	}

	public static void alertError(Component component, String action, Throwable exp){
		JOptionPane.showConfirmDialog(component,"An error occurred: "+exp.getClass().getSimpleName()+"\nMessage: "+exp.getMessage(), "Unable to "+action, JOptionPane.DEFAULT_OPTION ,JOptionPane.ERROR_MESSAGE);
		log.error("An error occurred during execution: ",exp);
	}


}
