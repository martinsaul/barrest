package com.zenithnetwork.desktop.system;

import javax.swing.*;
import java.net.URL;

public final class ProgramInfo {

	private static final String longProgramName = "BarREST API Tester";
	private static final String programName = "BarREST";
	private static final double version = 1.000;
	private static final int force = 0;
	private static final String icon = "icons/";
	private static final String logo = "logo/";
	private static String extension = ".jpg";
	private static ImageIcon iconP;
	private static long startupStamp;
	private static String subicon = "";
	private static final String title = programName + " " + version + " ("
			+ getCodeName() + ")";
	private static final String userConfigData = System
			.getProperty("user.home") + "\\." + programName;

	public static double getVersion() {
		return version;
	}

	public static String getProgramName() {
		return programName;
	}

	public static String getTitle(boolean a) {
		if (a)
			return title;
		else
			return programName + " " + version;
	}

	public static String getFullProgramName() {
		return longProgramName;
	}

	private static String getCodeName() {
		String a;
		switch (((int) (version * 1000))) {
		case 0:
			a = "First Run";
			break;
		case 1:
			a = "Nothing Works :'D";
			subicon = "Touch";
			extension = ".png";
			break;
		default:
			a = "Invalid Version";
		}
		return a;
	}

	public static String getGraphicalResources() {
		return "/imageFiles/";
	}

	public static String getSQLResources() {
		return "sql/";
	}

	public static String getXMLResources() {
		return "xml/";
	}

	public static String getIcon() {
		return getGraphicalResources() + icon + "icon" + subicon + extension;
	}

	public static String getLogo() {
		return getGraphicalResources() + logo + "logo.png";
	}

	public static void getImageIcon(URL a, JFrame b) {
		if (iconP == null) {
			iconP = new ImageIcon(a);
		}
		b.setIconImage(iconP.getImage());
	}

	public static void getImageIcon(URL a, JDialog b) {
		if (iconP == null) {
			iconP = new ImageIcon(a);
		}
		b.setIconImage(iconP.getImage());
	}

	public static String getUserConfigData() {
		return userConfigData;
	}

	public static void setInitTime() {
		startupStamp = System.currentTimeMillis();
	}

	public static long getStartupStamp() {
		return startupStamp;
	}

	public static int getCurrentForce() {
		return force;
	}
}
