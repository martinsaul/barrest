package com.zenithnetwork.desktop.launcher;

import com.zenithnetwork.desktop.barrest.Console;
import com.zenithnetwork.desktop.system.ParameterManager;
import com.zenithnetwork.desktop.system.ProgramManager;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Properties;
import java.util.Scanner;

public class Launcher {
	private static final org.slf4j.Logger log = LoggerFactory.getLogger(Launcher.class.getName());
	
	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			log.warn("Error changing look'n feel.",e);
			e.printStackTrace();
		}

		HashMap<String, String> parameters = new HashMap<>();
		
		Scanner scanner = new Scanner(toString(args));
		
		while(scanner.hasNext()){
			String parm = scanner.next();
			if(parm.startsWith("-") && scanner.hasNext()){
				parameters.put(parm.substring(1, parm.length()), scanner.next());
			}
			else if(parm.startsWith("+")){
				parameters.put(parm.substring(1, parm.length()), "true");
			}
			else{
				log.error("Ignored parameter: "+parm);
			}
		}

        File propFile = new File("parm.config");
        Properties properties = new Properties();
        try{
            FileInputStream fis = new FileInputStream(propFile);
            properties.load(fis);
			for (final String name: properties.stringPropertyNames())
				parameters.put(name, properties.getProperty(name));
            log.info("Configuration file loaded successfully.");
        } catch (FileNotFoundException ef){
            log.info("Configuration File does not exist.");
        } catch (Exception e){
            log.info("Failed to load Configuration File.",e);
        }

		ParameterManager.initialize(parameters);
		log.debug("Parameters loaded.");
		
		if(ParameterManager.getInstance().getBooleanParameter("update")){
			new File("updater.jar").delete();
		}
		
		Console.start();
	}

	public static String toString(String[] arg){
		StringBuilder builder = new StringBuilder();
		for(String s : arg) {
		    builder.append(s);
		    builder.append(' ');
		}
		return builder.toString();
	}
}
