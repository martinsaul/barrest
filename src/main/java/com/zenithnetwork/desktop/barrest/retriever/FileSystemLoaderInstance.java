package com.zenithnetwork.desktop.barrest.retriever;

import com.zenithnetwork.desktop.barrest.http.HttpResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ExecutionException;

public class FileSystemLoaderInstance implements LoaderInstance<FileSystemLoaderConfig>{
	private static final Logger log = LoggerFactory.getLogger(FileSystemLoaderInstance.class);

	private final HttpResponse response;
	private final FileSystemLoader fileSystemLoader;

	FileSystemLoaderInstance(HttpResponse response, FileSystemLoader fileSystemLoader) {
		this.response = response;
		this.fileSystemLoader = fileSystemLoader;
	}

	@Override
	public void load(FileSystemLoaderConfig config) {
		String contentFilePath = config.getFile().getAbsolutePath();
		try {
			if (fileSystemLoader.getLastModified().get(contentFilePath) != null && fileSystemLoader.getLastModified().get(contentFilePath) < config.getFile().lastModified())
				fileSystemLoader.getCache().refresh(contentFilePath);
			response.setPayload(fileSystemLoader.getCache().get(contentFilePath));
		} catch (ExecutionException e) {
			log.error("Exception happened retrieving file!", e);
			response.setHttpCode(500);
			response.setPayload("Exception happened retrieving file!");
		}

	}
}
