package com.zenithnetwork.desktop.barrest.retriever;

import com.zenithnetwork.desktop.barrest.http.HttpResponse;

public interface Loader<T extends LoaderInstance> {
	T setup(HttpResponse response);
}
