package com.zenithnetwork.desktop.barrest.retriever;

import java.io.File;

public class FileSystemLoaderConfig implements LoaderConfig{
	private final File file;

	public FileSystemLoaderConfig(File file) {
		this.file = file;
	}

	public File getFile() {
		return file;
	}
}
