package com.zenithnetwork.desktop.barrest.retriever;

public interface LoaderInstance<T extends LoaderConfig> {
	void load(T config);
}
