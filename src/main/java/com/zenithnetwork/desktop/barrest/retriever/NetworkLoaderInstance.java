package com.zenithnetwork.desktop.barrest.retriever;

import com.zenithnetwork.desktop.barrest.http.HttpResponse;
import okhttp3.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public class NetworkLoaderInstance implements LoaderInstance<NetworkLoaderConfig> {
	private static final Logger log = LoggerFactory.getLogger(NetworkLoaderInstance.class);
	private static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");

	private final HttpResponse response;
	private final NetworkLoader networkLoader;
	private final OkHttpClient client;

	public NetworkLoaderInstance(HttpResponse response, NetworkLoader networkLoader) {
		this.response = response;
		this.networkLoader = networkLoader;
		this.client = new OkHttpClient();
	}

	@Override
	public void load(NetworkLoaderConfig config) {
		if(config.isCache() && networkLoader.getCache().getIfPresent(config.getUrl()) != null){
			response.copy(networkLoader.getCache().getIfPresent(config.getUrl()));
		}
		else {
			//TODO Support stuff other than JSON.
			RequestBody requestBody = null;

			String method = config.getRequestMethod();
			if(method.equalsIgnoreCase("POST") || method.equalsIgnoreCase("PUT") || method.equalsIgnoreCase("PATCH")){
				requestBody = RequestBody.create(JSON, config.getRequestBody());
			}

			Request.Builder builder = new Request.Builder()
					.url(config.getUrl())
					.method(config.getRequestMethod(), requestBody);

			Request request = builder.build();
			try (Response httpResponse = client.newCall(request).execute()) {
				response.setPayload(httpResponse.body().string());
				response.setHttpCode(httpResponse.code());
				Map responseHeaders = httpResponse.headers().toMultimap();
				for(Object objKey: responseHeaders.keySet().toArray()){
					String key = objKey.toString();
					List<String> values = (List<String>) responseHeaders.get(key);
					String[] header = new String[values.size()+1];
					header[0] = key;
					for(int index = 0; index < values.size(); index++){
						header[index + 1] = values.get(index);
					}
					response.addHeader(header);
				}

			} catch (IOException e) {
				response.setPayload("Failed to retrieve payload from remote.");
				response.setHttpCode(500);
				log.error("Failed to retrieve payload from remote.", e);
			}
			if(config.isPersistCache()){
				//TODO
			}
		}
	}
}
