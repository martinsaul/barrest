package com.zenithnetwork.desktop.barrest.retriever;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.zenithnetwork.desktop.barrest.http.HttpResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

public class FileSystemLoader implements Loader<FileSystemLoaderInstance> {
	private final ConcurrentHashMap<String, Long> lastModified;
	private final LoadingCache<String, String> cache;

	public FileSystemLoader(){
		lastModified = new ConcurrentHashMap<>();
		cache = CacheBuilder.newBuilder()
				.maximumSize(1000)
				.expireAfterWrite(10, TimeUnit.MINUTES)
				.build(
						new CacheLoader<String, String>() {
							public String load(String key) throws IOException {
								File contentFile = new File(key);
								byte[] encoded = Files.readAllBytes(Paths.get(contentFile.getAbsolutePath()));
								lastModified.put(key, contentFile.lastModified());
								return new String(encoded);
							}
						});
	}

	public FileSystemLoaderInstance setup(HttpResponse response) {
		return new FileSystemLoaderInstance(response, this);
	}

	public ConcurrentHashMap<String, Long> getLastModified() {
		return lastModified;
	}

	public LoadingCache<String, String> getCache() {
		return cache;
	}

}
