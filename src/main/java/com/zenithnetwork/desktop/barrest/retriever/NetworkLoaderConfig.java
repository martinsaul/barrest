package com.zenithnetwork.desktop.barrest.retriever;

public class NetworkLoaderConfig implements LoaderConfig {
	private final String url;
	private final boolean cache;
	private final boolean persistCache;
	private final String requestBody;
	private final String requestMethod;

	public NetworkLoaderConfig(String url, boolean cache, boolean persistCache, String requestMethod, String requestBody) {
		this.url = url;
		this.cache = cache;
		this.persistCache = persistCache;
		this.requestBody = requestBody;
		this.requestMethod = requestMethod;
	}

	public String getUrl() {
		return url;
	}

	public boolean isCache() {
		return cache;
	}

	public boolean isPersistCache() {
		return persistCache;
	}

	public String getRequestMethod() {
		return requestMethod;
	}

	public String getRequestBody() {
		return requestBody;
	}
}
