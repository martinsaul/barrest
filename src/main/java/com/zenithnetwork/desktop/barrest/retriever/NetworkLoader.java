package com.zenithnetwork.desktop.barrest.retriever;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.zenithnetwork.desktop.barrest.http.HttpResponse;

import java.util.concurrent.TimeUnit;

public class NetworkLoader implements Loader<NetworkLoaderInstance>{
	private final Cache<String, HttpResponse> cache;

	public NetworkLoader(){
		cache = CacheBuilder.newBuilder()
				.maximumSize(1000)
				.expireAfterWrite(10, TimeUnit.MINUTES)
				.build();
	}

	@Override
	public NetworkLoaderInstance setup(HttpResponse response) {
		return new NetworkLoaderInstance(response, this);
	}

	public Cache<String, HttpResponse> getCache() {
		return cache;
	}
}
