package com.zenithnetwork.desktop.barrest.http;

import java.util.ArrayList;
import java.util.List;

public class HttpResponse {
	private  List<String[]> extraHeader;
	private int httpCode;
	private String payload;

	public HttpResponse(int httpCode, String payload) {
		this.httpCode = httpCode;
		this.extraHeader = new ArrayList<>();
		this.payload = payload;
	}

	public int getHttpCode() {
		return httpCode;
	}

	public void setHttpCode(int httpCode) {
		this.httpCode = httpCode;
	}

	public String getPayload() {
		return payload;
	}

	public void setPayload(String payload) {
		this.payload = payload;
	}

	public List<String[]> getExtraHeader() {
		return extraHeader;
	}

	public boolean addHeader(String[] header) {
		return extraHeader.add(header);
	}

	public void copy(HttpResponse response){
		this.httpCode = response.httpCode;
		this.extraHeader = response.extraHeader;
		this.payload = response.payload;
	}
}
