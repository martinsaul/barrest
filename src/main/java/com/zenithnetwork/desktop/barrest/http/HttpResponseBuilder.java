package com.zenithnetwork.desktop.barrest.http;

import com.sun.net.httpserver.HttpExchange;
import com.zenithnetwork.desktop.barrest.retriever.FileSystemLoader;
import com.zenithnetwork.desktop.barrest.retriever.FileSystemLoaderConfig;
import com.zenithnetwork.desktop.barrest.retriever.NetworkLoader;
import com.zenithnetwork.desktop.barrest.retriever.NetworkLoaderConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class HttpResponseBuilder {
	private static final Logger log = LoggerFactory.getLogger(HttpResponseBuilder.class);
	private static final File configFolder = new File("config/");

	private final FileSystemLoader fsLoader;
	private final NetworkLoader netLoader;

	public HttpResponseBuilder(){
		fsLoader = new FileSystemLoader();
		netLoader = new NetworkLoader();
	}

	public HttpResponse parse(HttpExchange httpRequest, String body) {
		int httpCode;
		List<String[]> extraHeader;
		String payload;

		String folderCompat = httpRequest.getRequestURI().getSchemeSpecificPart();
		folderCompat = folderCompat.replace('?', '#');
		folderCompat = folderCompat.replace('&', '#');
		File currentFolder = new File(configFolder, folderCompat);
		File current = new File(currentFolder, httpRequest.getRequestMethod() + ".txt");

		HttpResponse response = new HttpResponse(200, "Request logged.");

		//noinspection ResultOfMethodCallIgnored
		current.getParentFile().mkdirs();
		if (current.exists()) {
			Properties prop = new Properties();
			try {
				prop.load(new FileInputStream(current));
				if (prop.containsKey("httpResponse.content")) {
					response.setPayload(prop.getProperty("httpResponse.content"));
				}
				if (prop.containsKey("httpResponse.contentFile")) {
					FileSystemLoaderConfig config = new FileSystemLoaderConfig(new File(currentFolder, prop.getProperty("httpResponse.contentFile")));
					fsLoader.setup(response).load(config);
				}
				if (prop.containsKey("httpResponse.passThrough.url")) {
					String url = prop.getProperty("httpResponse.passThrough.url");
					boolean cache = Boolean.parseBoolean(prop.getProperty("httpResponse.passThrough.cache"));
					boolean persistCache = Boolean.parseBoolean(prop.getProperty("httpResponse.passThrough.persistCache"));
					NetworkLoaderConfig config = new NetworkLoaderConfig(url, cache, persistCache, httpRequest.getRequestMethod(), body);
					netLoader.setup(response).load(config);
				}

				if (prop.containsKey("httpResponse.httpCode"))
					response.setHttpCode(Integer.parseInt(prop.getProperty("httpResponse.httpCode")));

				for (Object key : prop.keySet()) {
					if (key.toString().startsWith("httpResponse"))
						continue;
					response.addHeader(new String[]{key.toString(), prop.get(key).toString()});
				}

			} catch (IOException e) {
				log.error("Failed to load config file.", e);
			}
		}

		return response;
	}
}
