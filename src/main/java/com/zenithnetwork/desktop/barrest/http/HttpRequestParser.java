package com.zenithnetwork.desktop.barrest.http;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

public class HttpRequestParser implements HttpHandler {
    private File folder = new File("log\\");
    private int connectionId = 0;
    private final HttpResponseBuilder responseBuilder;

    public HttpRequestParser(){
        //noinspection ResultOfMethodCallIgnored
        folder.mkdirs();
        responseBuilder = new HttpResponseBuilder();
    }

    @Override
    public void handle(HttpExchange t) throws IOException {
        connectionId++;

        String body = getBody(t);

        writeToFile(connectionId + ".txt", body);

        HttpResponse result = responseBuilder.parse(t, body);

        for(String[] header: result.getExtraHeader()){
            t.getResponseHeaders().add(header[0], header[1]);
        }
        t.sendResponseHeaders(result.getHttpCode(), result.getPayload().getBytes(Charset.forName("UTF-8")).length);
        OutputStream os = t.getResponseBody();
        os.write(result.getPayload().getBytes(Charset.forName("UTF-8")));
        os.close();
    }

    private String getBody(HttpExchange httpExchange) throws IOException {
        InputStreamReader isr =  new InputStreamReader(httpExchange.getRequestBody(), StandardCharsets.UTF_8);
        BufferedReader br = new BufferedReader(isr);

        int b;
        StringBuilder buf = new StringBuilder(512);
        while ((b = br.read()) != -1) {
            buf.append((char) b);
        }

        br.close();
        isr.close();

        String content = buf.toString();
        if(content.isEmpty())
            content=null;
        return content;
    }

    private void writeToFile(String name, String content) {
        try {
            PrintWriter writer = new PrintWriter(new File(folder,name), "UTF-8");
            writer.println(content);
            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
