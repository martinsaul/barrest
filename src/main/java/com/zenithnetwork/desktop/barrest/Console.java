package com.zenithnetwork.desktop.barrest;

import com.sun.net.httpserver.HttpContext;
import com.sun.net.httpserver.HttpServer;
import com.zenithnetwork.desktop.barrest.http.HttpRequestParser;
import com.zenithnetwork.desktop.system.ParameterManager;
import com.zenithnetwork.desktop.system.ProgramManager;

import java.io.IOException;
import java.net.InetSocketAddress;

public class Console {
    public static void start() {
        int port = ParameterManager.getInstance().getIntegerParameter("port",80);
        try {
            HttpServer server = HttpServer.create(new InetSocketAddress(port), 0);
            HttpContext context = server.createContext("/");
            context.setHandler(new HttpRequestParser());
            server.start();
            try {
                System.in.read();
                ProgramManager.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
